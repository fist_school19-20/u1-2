﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    Animator animator;
    CharController player;
    NavMeshAgent agent;

    [SerializeField] float AttackRange = 2;
    void Start()
    {
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (player != null)
        {
            // player inside trig
            agent.SetDestination(player.transform.position);
            if (Vector3.Magnitude(player.transform.position - transform.position) <= AttackRange)
            {
                agent.isStopped = true;
                animator.SetInteger("state", 3); // attack
            }
            else
            {
                agent.isStopped = false;
                animator.SetInteger("state", 2); // run
            }
        }
        else
        {
            agent.isStopped = true;
            animator.SetInteger("state", 0); // idle
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<CharController>() != null)
        {
            player = other.GetComponent<CharController>();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<CharController>() != null)
        {
            player = null;
        }
    }
}
