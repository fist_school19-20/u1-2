﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public Text label;
    public Text scoreText;
    public int score = 0;
    int total = 0;
    //private
    public GameObject terrain; // = null
    public GameObject finish;
    bool done = false;

    private void Start()
    {
        Teapot[] teapots = GameObject.FindObjectsOfType<Teapot>();
        for (int i = 0; i < teapots.Length; i++)
        {
            total += teapots[i].cost;
        }
        ChangeScore(0);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (done == false && collision.collider.gameObject == terrain)
        {
            label.enabled = true;
            done = true;
        }
    }
    void OnTriggerEnter(Collider other) {
        if (done == false && other.gameObject == finish) {
            label.text = "you win";
            label.color = new Color(1, 0.078f, 0.577f);
            label.enabled = true;
            done = true;

            FindObjectOfType<GameManager>().playerScore += score;
            FindObjectOfType<GameManager>().LoadLevel(
                SceneManager.GetActiveScene().buildIndex + 1
            );


        }
    }

    //void - формальный, пустой результат 
    //ОБЪЯВЛЕНИЕ МЕТОДА:
    //модиф. доступа
    //тип возвр. значения
    //имя метода
    // (набор входных аргументов)
    // { блок операций, тело метода }
    public void ChangeScore(int value)
    {
        score += value;
        scoreText.text = "Score: " + score + " / " + total;
    }
}
