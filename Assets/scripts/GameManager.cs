﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class GameManager : MonoBehaviour
{
    public int playerScore = 0;
    public string path;
    public LandscapeGenerator lg;
    private void Start()
    {
        path = Application.dataPath + "/save.txt";

        if (File.Exists(path))
            LoadWorld();
        else
            CreateWorld();
        DontDestroyOnLoad(gameObject);
    }
    public void LoadLevel(int index)
    {
        if (index >= SceneManager.sceneCountInBuildSettings)
        {
            string data = playerScore.ToString();
            File.WriteAllText(path, data);

            SceneManager.LoadScene(0);
            Destroy(gameObject);
        }
        else
        {
            SceneManager.LoadScene(index);
        }
    }
    public void ExitGame()
    {
        Application.Quit();
        print("Украине");
    }

    public void CreateWorld()
    {
        lg.Create();
    }
    public void LoadWorld()
    {
        string[] lines = File.ReadAllLines(path);
        lg.Load(lines);
    }
    public void SaveWorld()
    {
        List<string> lines = new List<string>();
        for (int i = 0; i < lg.transform.childCount; i++)
        //foreach (Transform block in lg.transform)
        {
            //string[] lines = new string[lg.transform.childCount];
            string line = lg.transform.GetChild(i).position.x + ";" +
                          lg.transform.GetChild(i).position.y + ";" +
                          lg.transform.GetChild(i).position.z + ";" +
                          lg.transform.GetChild(i).GetComponent<HP>().GetCurrentHP() + ";" +
                          lg.transform.GetChild(i).GetComponent<Block>().id;
            lines.Add(line);
        }
        File.WriteAllLines(path, lines);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F5))
        {
            SaveWorld();
        }
    }
}
