﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropHandler : MonoBehaviour,
    IDropHandler
{
    public void OnDrop(PointerEventData data)
    {
        if (transform.childCount == 0)
        {
            data.pointerDrag.GetComponent<DragHandler>()
                .startParent = transform;
        }
    }
}
