﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource : MonoBehaviour
{
    public int id;
    public string Name;
    public int Weight;
    public Sprite Icon;
}
