﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Block : MonoBehaviour
{
    public int id;
    public string Name;
    private void OnDestroy()
    {
        transform.parent.GetComponent<NavMeshSurface>().BuildNavMesh();
    }
}

