﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public RectTransform[] craftSlots;
    public CharController player;
    public void Craft()
    {
        if (craftSlots[0].childCount == 0)
        {
            print("error");
            return;
        }
        int id = craftSlots[0].GetChild(0).GetComponent<Resource>().id;
        foreach (RectTransform slot in craftSlots)
        {
            if (slot.childCount == 0 || slot.GetChild(0).GetComponent<Resource>().id != id)
            {
                print("error");
                return;
            }
        }
        player.ChangeMats(id, -4);
        player.ChangeBlocks(id, 1);

        ClearTable();
        print("craft");
    }
    public void ClearTable()
    {
        foreach (RectTransform slot in craftSlots)
        {
            if (slot.childCount > 0)
                Destroy(slot.GetChild(0).gameObject);
        }
    }
}
