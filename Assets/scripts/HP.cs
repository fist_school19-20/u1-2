﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HP : MonoBehaviour
{
    private int hp;
    [SerializeField] int maxHp;
    [SerializeField] GameObject destroyed;
    void Start()
    {
        hp = maxHp;
    }
    public void GetDamage(int dmg)
    {
        hp -= dmg;
        if (hp <= 0)
        {
            // spawn cracks
            //GameObject.Instantiate(destroyed, transform.position, transform.rotation, transform.parent);
            GameObject.Instantiate(destroyed, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
    public string GetInfo(Text hint)
    {
        if (hp <= maxHp * 0.1f)
            hint.color = Color.red;
        else
            hint.color = Color.white;
        return hp + " / " + maxHp;
    }
    public int GetCurrentHP()
    {
        return hp;
    }
}
