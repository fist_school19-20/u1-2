﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float v = Input.GetAxis("Horizontal");
        float h = Input.GetAxis("Vertical");

        Quaternion rot = transform.rotation;
        rot.x += h * 0.02f;
        rot.z -= v * 0.02f;
        transform.rotation = rot;
    }

}
