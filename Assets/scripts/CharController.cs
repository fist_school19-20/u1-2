﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson; //

public class CharController : MonoBehaviour
{
    public LandscapeGenerator lg;
    public Sprite[] sprites;
    public GameObject slotPrefab;
    public RectTransform content;

    bool target = false;
    public RectTransform selector;
    public RectTransform hotBar;
    public RectTransform inventory;
    FirstPersonController fpsc; 

    int[] resources;
    int[] blocks; ///
    int selectedBlock = 0;
    Camera cam;
    public Text hint;
    public GameObject[] prefabs;
    RaycastHit hit = new RaycastHit();
    public Transform weaponContainer;
    public Transform marker;

    public UIManager uim;
    void Start() {
        fpsc = GetComponent<FirstPersonController>(); //
        cam = GetComponentInChildren<Camera>();
        resources = new int[4]; // 0, 2, 4, 0 количествА ресурсов разных типов
        blocks = new int[4]; //
    }
    void Update() {
        if (Input.GetKeyDown(KeyCode.Tab))
        {        
            target = !inventory.gameObject.activeInHierarchy;
            inventory.gameObject.SetActive(target);
            if (target == true)
            {
                fpsc.enabled = false;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                Time.timeScale = 0;

                for (int i = 0; i < resources.Length; i++)// переключает типы ресурсов
                {
                    for (int j = 0; j < resources[i]; j++)// отсчитывает колво ресов j типа
                    {
                        GameObject s = GameObject.Instantiate(slotPrefab, content);

                        s.transform.GetChild(0).GetComponent<Image>().sprite = sprites[i];
                        s.transform.GetChild(0).GetComponent<Resource>().id = i;
                    }
                }              
            }
            else
            {
                fpsc.enabled = true;
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                Time.timeScale = 1;

                foreach (Transform slot in content)
                {
                    Destroy(slot.gameObject);
                }
                uim.ClearTable();
            }
        }
        if (target == true)
            return;

        Physics.Raycast(
            cam.transform.position, // координаты начала
            cam.transform.forward, // направление луча
            out hit, // данные о столкновении
            3.0f// дистанция взаимодействия
            );
        if (hit.collider != null)
        {
            hint.text = hit.collider.gameObject.name;
            if (hit.collider.gameObject.GetComponent<HP>())
            {
                hint.text +=
                    "\n" + hit.collider.gameObject
                    .GetComponent<HP>().GetInfo(hint);
            }
            if (hit.collider.tag == "Block")
            {
                marker.GetComponent<MeshRenderer>().enabled = true;
                marker.position = hit.collider.transform.position +
                    hit.normal;
            }
            else
            {
                marker.GetComponent<MeshRenderer>().enabled = false;
            }
        }
        else
        {
            hint.text = "";
            marker.GetComponent<MeshRenderer>().enabled = false;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (hit.collider.gameObject.GetComponent<Weapon>() != null)
            {
                DropWeapon();
                hit.collider.transform.SetParent(weaponContainer);
                weaponContainer.GetChild(0).localRotation = Quaternion.identity;
                weaponContainer.GetChild(0).localPosition = Vector3.zero;
                weaponContainer.GetChild(0)
                    .GetComponent<Rigidbody>().isKinematic = true;
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            weaponContainer.GetChild(0)
                .GetComponent<Animator>().SetTrigger("attack");
            // null -> bool => false
            // object ->bool => true
            if (hit.collider && hit.collider.gameObject.GetComponent<HP>())
            {
                hit.collider.gameObject
                .GetComponent<HP>().GetDamage(
                    weaponContainer.GetChild(0)
                    .GetComponent<Weapon>().Damage
                );
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            // build block
#warning проверка что маркер включен
            GameObject.Instantiate(
                prefabs[selectedBlock],
                marker.position,
                marker.rotation,
                lg.transform
                );
        }
        if (Input.GetKeyDown(KeyCode.Alpha1)) selectedBlock = 0;
        if (Input.GetKeyDown(KeyCode.Alpha2)) selectedBlock = 1;
        if (Input.GetKeyDown(KeyCode.Alpha3)) selectedBlock = 2;
        if (Input.GetKeyDown(KeyCode.Alpha4)) selectedBlock = 3;

        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            selectedBlock +=
                System.Math.Sign(Input.GetAxis("Mouse ScrollWheel"));
            // <0 => -1, 0 => 0, >0 => +1

            if (selectedBlock > prefabs.Length - 1)
                selectedBlock = 0; // some text
            if (selectedBlock < 0)
                selectedBlock = prefabs.Length - 1;
        }

        selector.SetParent(hotBar.GetChild(selectedBlock));
        selector.localPosition = Vector3.zero;
    }
    void DropWeapon()
    {
        if (weaponContainer.childCount == 0)
            return;
        weaponContainer.GetChild(0)
                    .GetComponent<Rigidbody>().isKinematic = false;
        weaponContainer.GetChild(0).SetParent(null);
    }
    public void ChangeMats(int id, int value)
    {
        resources[id] += value;
        hotBar.GetChild(id + 5).GetChild(1).GetComponent<Text>().text =
            resources[id].ToString();
    }
    public void ChangeBlocks(int id, int value)
    {
        blocks[id] += value;
        hotBar.GetChild(id).GetChild(1).GetComponent<Text>().text =
            blocks[id].ToString();
    }
}
