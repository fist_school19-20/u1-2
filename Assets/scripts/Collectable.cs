﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    public int id; // id -> индекс в массиве инв.
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<CharController>() != null)
        {
            other.gameObject.GetComponent<CharController>().ChangeMats(id, 1);
            Destroy(gameObject);
        }   
    }
}
