﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teapot : MonoBehaviour
{
    public int cost;
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GetComponent<Animator>().Play("collect");

            // ВЫЗОВ МЕТОДА ChangeScore
            other
                .gameObject
                .GetComponent<PlayerController>()
                .ChangeScore(cost);
            Destroy(gameObject, 1f);
            //GetComponent<T>()
        }
    }
}
