﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LandscapeGenerator : MonoBehaviour
{
    public GameObject[] prefabs;
    public int size;
    public int maxHeight;
    private void Start()
    {
     //   Create();
    }
    public void Create()
    {
        int seed = Random.Range(0, 256);
        for (int j = 0; j < size; j++)
        {
            for (int i = 0; i < size; i++)
            {
                int y = (int)
                    (maxHeight*Mathf.PerlinNoise(seed + i / 32f, seed + j / 32f));
                //for (int k = y; k >= 0; k--)
                //{
                    GameObject go = GameObject.Instantiate(
                        prefabs[(int)(y * prefabs.Length * (1f / maxHeight))],
                        new Vector3(i, y, j), // 0,0,0
                        Quaternion.identity, // 0,0,0
                        transform
                        );
                   // if (k != y) go.SetActive(false);
               // }
            }
        }
        GetComponent<NavMeshSurface>().BuildNavMesh();
    }
    public void Load(string[] data)
    {
        foreach (string line in data)
        {
            string[] tmp = line.Split(';');
            int x = int.Parse(tmp[0]);
            int y = int.Parse(tmp[1]);
            int z = int.Parse(tmp[2]);
            int hp = int.Parse(tmp[3]);
            int id = int.Parse(tmp[4]);

            GameObject go = GameObject.Instantiate(
                        prefabs[id],
                        new Vector3(x, y, z), // 0,0,0
                        Quaternion.identity, // 0,0,0
                        transform
                        );
        }
        GetComponent<NavMeshSurface>().BuildNavMesh();
    }
}
